@extends ('layouts.master')
	@section ('content')

		<div class="col-sm-8 blog-main" >
			<h1>Create post</h1>
			
			<hr>

			<form method= "POST" action="/posts">
				{{csrf_field()}}
			    <div class="form-group row">
			    	<label for="title" class="col-sm-2 col-form-label">Title : </label>
			    	<div class="col-sm-10">
			        	<input type="text" class="form-control" id="title" name="title" placeholder="My big idea" >
			      	</div>
			    </div>
			    
			    <div class="form-group row">
			      <label for="body" class="col-sm-2 col-form-label">Body : </label>
			      <div class="col-sm-10">
			        <textarea type="text" class="form-control" id="body" name="body" placeholder="Content of the post" ></textarea>
			      </div>
			    </div>

			    <div class="form-group row">
			      <div class="offset-sm-2 col-sm-10">
			        <button type="submit" class="btn btn-primary">Publish</button>
			      </div>
			    </div>
			    @include ('layouts.formErrors')
		  </form>
		</div>
	@endsection