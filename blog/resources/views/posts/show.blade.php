@extends('layouts.master')

@section('content')
	<div class= "col-sm-8 blog-main">
			
		<h1>{{$post->title}}</h1>
	
		{{$post->body}}
		<hr>
		<div>
			<ul class="list-group">
				@foreach ($post->comments as $comment)
					<article>
						<li class = "list-group-item">
							<strong>
								{{$comment->created_at->diffForHumans()}} : &nbsp;

							</strong>
							
								{{$comment->body}}
						</li>


					</article>
				@endforeach
			</ul>
			{{-- Add a comment--}}
			<hr>
			<div class="card">
				<div class="card-block">	
					<form method= "POST" action="/posts/{{$post->id}}/comment">
						{{csrf_field()}}
					    <div class="form-group">
					       	<textarea name="body" placeholder="Leave us a comment" class="form-control"></textarea>
					    </div>
					    <div class="form-group">
					    	<button type="submit" class="btn btn-primary">Add comment</button>

					    </div>
					    @include ('layouts.formErrors')
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection