<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
    // the line below gaurds against POSTS or submissions with the fields in the array
    // e.g. protectet $guarded = ['user_id'] will prevent the back end to accept that field
    protected $guarded = [''];
    //be wary, we are leaivng it empty hence we are not restricting the submission
}
