<?php

namespace App\Http\Controllers;

use App\Task;


class TasksController extends Controller
{
	public function index()
    {
	
	    // query $tasks = DB::table('tasks')->get();
		$tasks = Task::all();    

		return view ('tasks.index', compact('tasks'));

	}



	//route model binding effectively uses Task $task as Task::find($id)
	public function show(Task $task) 
	{


 		

		return view ('tasks.show', compact('task'));

    	//return view('welcome', compact('tasks'));
	    
	}
}
