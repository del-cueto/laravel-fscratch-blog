<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    //scope prefix on the function allows us to add more queries to the transacion e.g. Task:Incomplete()->where('foo','bar')->get()'
    public function scopeIncomplete($query) {

    	return $query->where('completed', 0);


    }


}
